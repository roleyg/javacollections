package com.java2novice.treeset;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class treeset {
    public static void main(String a[]){
        List<String> li = new ArrayList<String>();
        li.add("Gareth=1");
        li.add("Izzy=2");
        li.add("Asher=3");
        li.add("Oaklyn=4");
        System.out.println("List: "+li);
        //create a treeset with the list
        TreeSet<String> myset = new TreeSet<String>(li);
        System.out.println("Set: "+myset);
    }
}
