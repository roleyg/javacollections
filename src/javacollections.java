import java.util.*;

class javacollections {
    public static void main(String[] args)
    {
        ArrayList<String> list = new ArrayList<String>();

        //Enter data
        list.add("Gareth");
        list.add("Izzy");
        list.add("Asher");
        list.add("Oaklyn");

        // Utilize iterator
        Iterator iterator = list.iterator();

        System.out.println("List of family members : ");

        while (iterator.hasNext())
            System.out.print(iterator.next() + " ");

        System.out.println();
    }
}
