package com.java2novice.hashset;

import java.util.HashSet;

public class hashset {

    public static void main(String a[]){
        HashSet<String> hs = new HashSet<String>();
        //add elements to HashSet
        hs.add("Gareth");
        hs.add("Izzy");
        hs.add("Asher");
        hs.add("Oaklyn");
        System.out.println(hs);
        System.out.println("Is HashSet empty? "+hs.isEmpty());
        hs.remove("first");
        System.out.println(hs);
        System.out.println("Size of the HashSet: "+hs.size());
        System.out.println("Does HashSet contains first element? "+hs.contains("first"));
    }
}