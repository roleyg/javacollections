import java.util.SortedMap;
import java.util.TreeMap;

public class treemap {
    public static void main(String[] args) {
        // Creating a TreeMap
        SortedMap<String, String> fileExtensions  = new TreeMap<>();

        // Adding new key-value pairs to a TreeMap
        fileExtensions.put("Gareth", "dad");
        fileExtensions.put("Izzy", "mom");
        fileExtensions.put("Asher", "son");
        fileExtensions.put("Oaklyn", "daughter");

        // Printing the TreeMap
        System.out.println(fileExtensions);
    }

}